var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var todoSchema = new Schema({
    title: { type: String, default: ""},
    status:{ type: String, default: "active" },
  },{
    timestamps: true
});

module.exports = mongoose.model('todo', todoSchema);