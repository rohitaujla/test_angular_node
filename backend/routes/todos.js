var express = require('express');
var router = express.Router();
var todotable = require("../models/todos");
var sanitize = require("mongo-sanitize");

router.post('/', function(req,res) {
    try {
        if (!req.body.title) {
            throw ({message: 'Missing required parameter: title'})
        }
        var todo = new todotable();
        todo.title = sanitize(req.body.title.trim());
        todo.save((save_err, response) => {
            if (save_err) {
                return res.send({status: false, message: save_err.mesage || save_err, data: []})
            }
            return res.send({status: true, message: "successfully insert data", data: response})
        })

    } catch (err) {
        return res.send({status: false, message: err.message || err, data: []})
    }
})
router.put('/:id', function(req,res) {
    try {
        if (!req.params.id || req.params.id == "") {
            throw ({mesage: "Missing required parameter: id"})
        }
        var body = {}
        if (req.body.status && (req.body.status == "active" || req.body.status == "inactive")) {
            body["status"] = req.body.status;
        }
        if (req.body.title) {
            body["title"] = sanitize(req.body.title.trim());
        }
        todotable.findByIdAndUpdate(req.params.id, body, {new: true} , function(update_err, response) {
          if (update_err) {
            return res.send({status: false, message: update_err.mesage || update_err})
          }
          return res.send({status: true, message: "todo update successfully ", data: response})
        });
    } catch (err) {
        return res.send({status: false, message: err.mesage || err})
    }
})
router.get('/', function(req, res, next) {
    todotable.find({}).exec((err, response) =>{
        try {
            if (err) {
                throw err;
            }
            return res.send({status: true, message: "fetch list successfully", data: response})
        } catch (err) {
            return res.send({status: false, message: err.mesage || err, data: []})
        }
    })
});
router.get('/:id', function(req, res, next) {
    todotable.findById(req.params.id).exec((err, response) =>{
        try {
            if (err) {
                throw err;
            }
            return res.send({status: true, message: "fetch list successfully", data: response})
        } catch (err) {
            return res.send({status: false, message: err.mesage || err, data: []})
        }
    })
});
router.delete("/:id", function(req, res, next) {
    try {
        if (!req.params.id || req.params.id == "") {
            throw ({mesage: "Missing required parameter: id"})
        }
        todotable.findByIdAndRemove(req.params.id, (delete_err, todo) => {
            if (delete_err) {
                return res.send({status: false, message: delete_err.mesage || delete_err})
              }
              return res.send({status: true, message: "todo delete successfully ", data: todo})
        });
    } catch (err) {
        return res.send({status: false, message: err.mesage || err})
    }
});
module.exports = router;
