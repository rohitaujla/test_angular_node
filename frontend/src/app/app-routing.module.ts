import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoComponent } from './todo/todo.component';
import { AddComponent } from './todo/add/add.component';



const routes: Routes = [
  {path:'', component:TodoComponent},
  {path:'list', component:TodoComponent},
  {path:'todo-add', component:AddComponent},
  {path:'todo-add/:id', component:AddComponent},
  {path:'**', component:TodoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
