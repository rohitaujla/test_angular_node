import { Component, OnInit } from '@angular/core';
import {TodoService} from '../todo.service';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';


@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  private todo_list = [];
  constructor(private todoService: TodoService, private toastr: ToastrService) { }

  ngOnInit() {
    this.todoService.fetchTodoList().subscribe((result) => {
         if(!result.status){
          this.toastr.error(result.message);
          return;
         }
         this.toastr.success(result.message);
         this.todo_list = (result.data.length > 0) ? result.data : [] ;
    });
  }
  changeTime(created_at: Date) {
    return moment.utc(created_at).local().format("MMM Do,YYYY hh:mm A")

  }
  deleteTodo(item_id, index) {
    this.todoService.deleteTodo(item_id).subscribe((result) => {
      if(!result.status){
        this.toastr.error(result.message);
        return;
       }
       this.toastr.success(result.message);
       this.todo_list.splice(index,1);
    })  
  }
  changeStatus(id, status,index) {
    let body = {status: ""}
    if(status == "active") {
      body["status"] = "inactive";
    } 
    if(status == "inactive") {
      body["status"] = "active";
    } 
    this.todoService.updateTodo(id, body).subscribe((result) => {
      if(!result.status){
        this.toastr.error(result.message);
        return;
       }
       this.toastr.success(result.message);
       this.todo_list[index].status = (body.status != "") ? body.status : status;
    })  
  }
  

}
