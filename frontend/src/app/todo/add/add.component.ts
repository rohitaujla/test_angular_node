import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TodoService} from '../../todo.service';
import { ToastrService } from 'ngx-toastr';
import {Router, ActivatedRoute} from "@angular/router"

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  form = new FormGroup({
    title: new FormControl('',[Validators.required, Validators.minLength(2), Validators.maxLength(10)]),
  });
  item_id = null ;
  constructor(private todoService: TodoService, private toastr: ToastrService, private router: Router, private activeRoute: ActivatedRoute) {
    this.activeRoute.params.subscribe(params => {
      if (params["id"]) {
        this.item_id = params["id"] ;
        this.fetchRecord();
      }
    })
   }

  ngOnInit() {

   
  }
  onSubmit(): void {
    if (!this.form.valid) {
      return;
    }
    if (this.item_id) {
      this.todoService.updateTodo(this.item_id, this.form.value).subscribe((result) => {
        if(!result.status){
          this.toastr.error(result.message);
          return;
         }
         this.toastr.success(result.message);
         this.router.navigate(['/list'])
      })  

    } else {
      this.todoService.createTodo(this.form.value).subscribe((result) => {
        if(!result.status){
          this.toastr.error(result.message);
          return;
         }
         this.toastr.success(result.message);
         this.router.navigate(['/list'])
      })  

    }
    
  }
  fetchRecord() {
    if(!this.item_id) {
      return;
    }
    this.todoService.fetchSingleTodo(this.item_id).subscribe((result) => {
      if(!result.status){
       return;
      }
      let title = result.data.title ;
      this.form.controls['title'].setValue(title);
 });
  }

}
