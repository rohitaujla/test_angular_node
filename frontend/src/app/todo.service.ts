import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import {SharingComponent} from './sharing/sharing.component';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private time_zone = new Date().getTimezoneOffset();
  private httpOptions = {
    headers: new HttpHeaders({ 'TimeZone': ''+this.time_zone })
  };

  constructor(private http: HttpClient, private sharingComponent:SharingComponent) { }
  
  fetchTodoList(): Observable<any> {
    return this.http.get(this.sharingComponent.localUrl + 'api/todos',this.httpOptions);
  }
  createTodo(body:any): Observable<any> {
    return this.http.post(this.sharingComponent.localUrl + 'api/todos', body, this.httpOptions);
  }
  deleteTodo(id:String): Observable<any> {
    return this.http.delete(this.sharingComponent.localUrl + 'api/todos/'+id, this.httpOptions);
  }
  updateTodo(id:String ,body:any): Observable<any> {
    return this.http.put(this.sharingComponent.localUrl + 'api/todos/'+id, body, this.httpOptions);
  }
  fetchSingleTodo(id): Observable<any> {
    return this.http.get(this.sharingComponent.localUrl + 'api/todos/'+id, this.httpOptions);
  }
}
